#!/bin/bash

aws --version >/dev/null 2>&1 || { echo >&2 "I require AWS Cli Util but it's not installed. Aborting :("; exit 1; }
node -v >/dev/null 2>&1 || { echo >&2 "I require Node Virtual Environment util but it's not installed. Aborting :("; exit 1; }
npm -v >/dev/null 2>&1 || { echo >&2 "I require Node Package Manager util but it's not installed. Aborting :("; exit 1; }
python --version >/dev/null 2>&1 || { echo >&2 "I require Python Virtual Environment util but it's not installed. Aborting :("; exit 1; }
pip --version >/dev/null 2>&1 || { echo >&2 "I require Python Package Manager util but it's not installed. Aborting :("; exit 1; }

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Environment vars
export ENV="LAB"
export AWS_PAGER=""
export AWS_DEFAULT_REGION=us-east-1
export AWS_DEFAULT_PROFILE=mapfre

# Generate the dump from pst to txt & attachments
cd pst_parser
npm start
cd ..

# TODO move folder into a previous level

cd _data/output/raw
aws s3 sync . s3://smartemail-us-east-1-888551519281-data/sample/raw/
cd -

# TODO