import os
import yaml
import errno

from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (PDFInfoNotInstalledError, PDFPageCountError, PDFSyntaxError)

# https://stackoverflow.com/questions/541390/extracting-extension-from-filename-in-python
# https://github.com/aws-samples/amazon-textract-code-samples/tree/master/python
# https://pypi.org/project/pdf2image/
# https://stackoverflow.com/questions/3451111/unzipping-files-in-python
# https://github.com/python-pillow/Pillow
# https://pypi.org/project/Wand/

rootdir = '/Users/mdepaz/Workspace/Mapfre/study_utils/_data/output/raw'
outputdir = '/Users/mdepaz/Workspace/Mapfre/study_utils/_data/output/classified'
with open(r'mapping.yaml') as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

def copy(*, file, path):
    print('TODO copy')

def discard(*, file, path):
    pass

def explode(*, file, path):

    print('explode')
    print('file? '+file)
    print('path? '+path )
    print('outputdir? '+outputdir )
    print('join? ' + os.path.join(outputdir, path))

    # try to make output partial path
    try:
        os.mkdir(os.path.join(outputdir, path))
        input_file = os.path.join(rootdir, path, file)
        output_file = os.path.join(outputdir, path, file)

        print(input_file)
        print(output_file)

        images = convert_from_bytes(
            open(input_file, 'rb').read(),
            output_folder=output_file,
            use_cropbox=False,
            thread_count=4,
            dpi=500,
            fmt='png'
        )

    except OSError as exc:
        if exc.errno != errno.EEXIST:
            print (exc)
            raise
        pass

    except (PDFInfoNotInstalledError, PDFPageCountError, PDFSyntaxError) as e:
        print ('Problems with PDF library and/or its dependencies')

def extract(*, file, path):
    print('TODO extract')

if __name__ == "__main__":

    extension_lists = []
    files_parsed = 0

    for subdir, dirs, files in os.walk(rootdir):
        for file in files:

            filename, file_extension = os.path.splitext(file)
            file_extension = file_extension.lower()
            fullpath = os.path.join(subdir, file)
            path = subdir.replace(rootdir, '')
            func = ('discard', config['mappings'].get(file_extension))[len(file_extension)>0]
            locals()[func](file=file, path=path)

            extension_lists.append(file_extension.lower())
            files_parsed += 1

            #print ('**************************')
            #print ('fullpath? ' + fullpath)
            #print ('subdir? ' + subdir)
            #print ('path? ' + path)
            #print ('file? ' + file)
            #print ('filename? ' + filename)
            #print ('file_extension? ' + file_extension)
            #print ('func to execute? ' + func)
            #print('**************************')

    print('Done!')
    print('Files processed:' + files_parsed)
    print('Extensions processed:' + set(extension_lists))


