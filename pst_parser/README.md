# PST parser

This project parses a list of pst files from input/pst/<*.pst> folder to ouput/raw/<file>/<message>/*.*

## Dependencies

* Docker
* Docker-compose
* Unix-Shell bash style

## HowTo Transform

```bash

    # How to use
    $> ./setup.sh
```

## HowTo Develop?

In order to develop, we must have a valid node.js & npm environment.

```bash
    
    $> npm i          # To install dependencies
    $> npm start      # To run
```
