#!/bin/bash

docker -v >/dev/null 2>&1 || { echo >&2 "I require Docker VM Environment but it's not installed. Aborting :("; exit 1; }
docker-compose -v >/dev/null 2>&1 || { echo >&2 "I require Docker Compose util but it's not installed. Aborting :("; exit 1; }

timestamp=`date '+%Y_%m_%d__%H_%M_%S'`;
docker-compose up > "extract_$timestamp.log"

echo "finished!"
