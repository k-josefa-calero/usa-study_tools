import * as fs from 'fs'
import { PSTAttachment, PSTMessage, PSTRecipient, PSTFile, PSTFolder } from 'pst-extractor'
import { resolve } from 'path'

const pstFolder = process.env.INPUT_DIR || resolve('../_data/input/pst')
const topOutputFolder = process.env.OUTPUT_DIR || resolve('../_data/output/raw/')

const whitelist = ['claimsmail@mapfreusa.com']

const saveToFS = true                   // Save to the output folder with: <outputFolder>/<psf_file>/<nodeId>/*.*
const displaySender = true              // Print the senderEmailAddress
const displayBody = false               // Print the content of email in plain text
const debugEmails = false               // Iterate over emails fields (only print)
const verbose = false                   // Verbose mode (reading text or drawing a progress bar based in points)

let outputFolder = ''
let depth = -1
let col = 0

// 1. make a top level folder to hold content
try {
    if (saveToFS) {
        fs.mkdirSync(topOutputFolder)
    }
} catch (err) {
    console.error(err)
}

// 2. Walk over the input folder
const directoryListing = fs.readdirSync(pstFolder)
directoryListing.forEach((filename) => {

    // time for performance comparison to Java and improvement
    const start = Date.now()

    // load file into memory buffer, then open as PSTFile
    const pstFile = new PSTFile(fs.readFileSync(pstFolder + '/' + filename))

    // make a sub folder for each PST
    try {
        if (saveToFS) {
            outputFolder = topOutputFolder + '/' + filename
            fs.mkdirSync(outputFolder)
        }
    } catch (err) {
        console.error(err)
    }

    console.log(pstFile.getMessageStore().displayName)
    processFolder(pstFile.getRootFolder())

    const end = Date.now()
    console.log('processed in ' + (end - start) + ' ms')
})

// Utility functions ///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Walk the folder tree recursively and process emails.
 * @param {PSTFolder} folder
 */
function processFolder(folder: PSTFolder): void {
    depth++

    // the root folder doesn't have a display name
    if (depth > 0) {
        console.log(getDepth(depth) + folder.displayName)
    }

    // go through the folders...
    if (folder.hasSubfolders) {
        const childFolders: PSTFolder[] = folder.getSubFolders()
        for (const childFolder of childFolders) {
            processFolder(childFolder)
        }
    }

    // and now the emails for this folder
    if (folder.contentCount > 0) {
        depth++
        let email: PSTMessage = folder.getNextChild()
        while (email != null) {

            // debug emails
            if (debugEmails) {
                debugEmail(email)
            }

            // Obtain data from email
            const sender = getSender(email)
            const recipients = getRecipients(email)

            // Print a progress bar or a line with the abstract of message
            if (verbose) {
                console.log(getDepth(depth) + 'Email: ' + email.descriptorNodeId + ' - ' + email.subject)
            } else {
                printDot()
            }

            // display body?
            if (verbose && displayBody) {
                console.log(email.body)
                console.log(email.bodyRTF)
            }

            // Check the whitelist filter before to save
            const whitelistACK = recipients.split(';').some(recipient=> whitelist.indexOf(recipient) >= 0)

            // save content to fs?
            if (saveToFS && whitelistACK) {
                // create date string in format YYYY-MM-DD
                let strDate = ''
                let d = email.clientSubmitTime
                if (!d && email.creationTime) {
                    d = email.creationTime
                }
                if (d) {
                    const month = ('0' + (d.getMonth() + 1)).slice(-2)
                    const day = ('0' + d.getDate()).slice(-2)
                    strDate = d.getFullYear() + '-' + month + '-' + day
                }

                // create a folder for each day (client submit time)
                let emailFolder = outputFolder + '/' + strDate + '/'
                if (!fs.existsSync(emailFolder)) {
                    try {
                        fs.mkdirSync(emailFolder)
                    } catch (err) {
                        console.error(err)
                    }
                }

                // create a folder for each email (descriptorNodeId)
                emailFolder += email.descriptorNodeId + '/'
                if (!fs.existsSync(emailFolder)) {
                    try {
                        fs.mkdirSync(emailFolder)
                    } catch (err) {
                        console.error(err)
                    }
                }

                doSaveToFS(email, emailFolder, sender, recipients)
            }
            email = folder.getNextChild()
        }
        depth--
    }
    depth--
}

/**
 * Returns a string with visual indication of depth in tree.
 * @param {number} depth
 * @returns {string}
 */
function getDepth(depth: number): string {
    let sdepth = ''
    if (col > 0) {
        col = 0
        sdepth += '\n'
    }
    for (let x = 0; x < depth - 1; x++) {
        sdepth += ' | '
    }
    sdepth += ' |- '
    return sdepth
}

/**
 * Get the sender and display.
 * @param {PSTMessage} email
 * @returns {string}
 */
function getSender(email: PSTMessage): string {
    let sender = email.senderName
    if (sender !== email.senderEmailAddress) {
        sender += ' (' + email.senderEmailAddress + ')'
    }
    if (verbose && displaySender && email.messageClass === 'IPM.Note') {
        console.log(getDepth(depth) + ' sender: ' + sender)
    }
    return sender
}

/**
 * Get the recipients and display.
 * @param {PSTMessage} email
 * @returns {string}
 */
function getRecipients(email: PSTMessage): string {

    // Mode A (optimal but with several errors due to use names instead addresses)
    // could walk recipients table, but be fast and cheap
    //return email.displayTo

    // Mode B (complex but more precise)
    const total = email.numberOfRecipients
    const addresses = []

    if (email.numberOfRecipients>0) {

        for (let i=total; i>0; i--) {
            const recipient:PSTRecipient = email.getRecipient(0)
            const cleanedAddress = recipient.emailAddress.toLowerCase().trim()
            addresses.push(cleanedAddress)
        }
    }

    if (addresses.length===0 && email.numberOfRecipients===0) {
        console.log('Email mágico con 0 destinatarios: será un borrador')
    }

    if (addresses.length !== email.numberOfRecipients) {
        console.log('Problems grabbing address')
        process.exit(1)
    }

    return addresses.join(';')
}

/**
 * Print a dot representing a message.
 */
function printDot(): void {
    process.stdout.write('.')
    if (col++ > 100) {
        console.log('')
        col = 0
    }
}

/**
 * Save items to filesystem.
 * @param {PSTMessage} msg
 * @param {string} emailFolder
 * @param {string} sender
 * @param {string} recipients
 */
function doSaveToFS(msg: PSTMessage, emailFolder: string, sender: string, recipients: string): void {
    try {
        // save the msg as a txt file
        const filename = emailFolder + msg.descriptorNodeId + '.txt'
        if (verbose) {
            console.log('saving msg to ' + filename)
        }
        const fd = fs.openSync(filename, 'w')
        fs.writeSync(fd, msg.clientSubmitTime + '\r\n')
        fs.writeSync(fd, 'Type: ' + msg.messageClass + '\r\n')
        fs.writeSync(fd, 'From: ' + sender + '\r\n')
        fs.writeSync(fd, 'To: ' + recipients + '\r\n')
        fs.writeSync(fd, 'Subject: ' + msg.subject)
        fs.writeSync(fd, msg.body)
        fs.closeSync(fd)
    } catch (err) {
        console.error(err)
    }

    // walk list of attachments and save to fs
    for (let i = 0; i < msg.numberOfAttachments; i++) {
        const attachment: PSTAttachment = msg.getAttachment(i)
        // Log.debug1(JSON.stringify(activity, null, 2));
        if (attachment.filename) {
            const filename =
                emailFolder + msg.descriptorNodeId + '-' + attachment.longFilename
            if (verbose) {
                console.log('saving attachment to ' + filename)
            }
            try {
                const fd = fs.openSync(filename, 'w')
                const attachmentStream = attachment.fileInputStream
                if (attachmentStream) {
                    const bufferSize = 8176
                    const buffer = Buffer.alloc(bufferSize)
                    let bytesRead
                    do {
                        bytesRead = attachmentStream.read(buffer)
                        fs.writeSync(fd, buffer, 0, bytesRead)
                    } while (bytesRead == bufferSize)
                    fs.closeSync(fd)
                }
            } catch (err) {
                console.error(err)
            }
        }
    }
}

/**
 * Get an email and print properties to debug
 * @param {PSTMessage} email
 */
function debugEmail(email: PSTMessage): void {

    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')

    // Oldie but goldie

    const recipients = email.displayTo
    const recipientsList = recipients.toLowerCase().split(';').map(r => r.trim())
    console.log(`pass filter? ${recipientsList.some(recipient=> whitelist.indexOf(recipient) >= 0)}`)      // Boolean

    console.log(`email.displayTo: ${email.displayTo}`)                              // i.e. ".ECCPoliceReports@mapfreusa.com" or 'Claims Mail'
    console.log(`email.displayCC: ${email.displayCC}`)                              // i.e. ".ECCPoliceReports@mapfreusa.com" or 'Claims Mail'
    console.log(`email.displayBCC: ${email.displayBCC}`)                            // i.e. ".ECCPoliceReports@mapfreusa.com" or 'Claims Mail'
    console.log(`email.receivedByAddressType: ${email.receivedByAddressType}`)      // i.e. "EX"
    console.log(`email.receivedByName: ${email.receivedByName}`)                    // i.e. "zzd ACTP Innovación EmailManagement"

    // Through complex object

    if (email.numberOfRecipients>0) {

        const firstRecipient:PSTRecipient = email.getRecipient(0)

        console.log(`email.numberOfRecipients: ${email.numberOfRecipients}`)
        console.log(`email.numberOfRecipients[0]->recipient: ${firstRecipient.emailAddress}`)

        //return ''
    }

    console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<')
}
