# Tools for the USA Study

A toolchain to operate and transform emails 

* PST_PARSER: Extracts backups in pst to attachment files (dockerized).
* PAGE_SEPARATOR: Converts all pdf into images splitted by page.
* textract_batch_parser: Uses the parser services onto all images available in a folder structure.
