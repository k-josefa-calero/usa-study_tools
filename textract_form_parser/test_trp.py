
import json
import sys
sys.path.append('..')

from lib.trp import Document

FECHA_OCURRENCIA = "fecha_de_ocurrencia"
PROVINCIA_ACCIDENTE = "provincia_de_ocurrencia"
MATRICULA_ASEGURADO = "matricula_asegurado"
MATRICULA_CONTRARIO = "matricula_contrario"

NOMBRE_ASEGURADO = "nombre_asegurado"
ENTIDAD_ASEGURADORA_ASEGURADO = "entidad_aseguradora_asegurado"
MARCA_VEHICULO_ASEGURADO = "marca_modelo_vehiculo"
MARCAMODELO_VEHICULO_ASEGURADO = "marcamodelo_vehiculo"
ENTIDAD_ASEGURADORA_CONTRARIO = "entidad_aseguradora_contrario"

DON_DONA = "don_dona"
DON_DOFIA = "don_dofia"
ENTIDAD_ASEGURADORA = "entidad_aseguradora"
MATRICULA = "matricula"
FDO_ = "Fdo_"


def processDocument(doc):
    found_fields = {}
    for page in doc.pages:
        found_fields_by_page = {}
        for field in page.form.fields:
            k = ""
            v = ""
            if field.key:
                k = rename_key(field, page, found_fields_by_page)
            if field.value:
                v = field.value.text
                a = getPosition(doc, v)
            if field.key and field.value:
                found_fields_by_page[k] = v
        found_fields.update(found_fields_by_page)
    return found_fields


def getPosition(doc, text_search):
    lines = list(filter(lambda el: el.get('Text') == text_search, filter(lambda b: "LINE" in b.get('BlockType'), doc.blocks[0]['Blocks'])))
    return filter(lambda el: el.get('Text') == text_search,)


def rename_key(field, page, found_fields_by_page):
    k = field.key.text
    k = k.replace(":", '').lower().replace(' ', '_').replace('/', '_')
    if "En caso de tratarse de Empresa, es obligatorio el sello de la misma" in page.getTextInReadingOrder():
        if k == MATRICULA and MATRICULA_CONTRARIO in found_fields_by_page.keys():
            k = MATRICULA_ASEGURADO
        elif k == MATRICULA and MATRICULA_CONTRARIO not in found_fields_by_page.keys():
            k = MATRICULA_CONTRARIO
    else:
        if k == MATRICULA and MATRICULA_ASEGURADO in found_fields_by_page.keys():
            k = MATRICULA_CONTRARIO
        elif k == MATRICULA and MATRICULA_ASEGURADO not in found_fields_by_page.keys():
            k = MATRICULA_ASEGURADO

    if "(Sello de Empresa)" in page.getTextInReadingOrder():
        if k == ENTIDAD_ASEGURADORA and ENTIDAD_ASEGURADORA_CONTRARIO in found_fields_by_page.keys():
            k = ENTIDAD_ASEGURADORA_ASEGURADO
        elif k == ENTIDAD_ASEGURADORA and ENTIDAD_ASEGURADORA_CONTRARIO not in found_fields_by_page.keys():
            k = ENTIDAD_ASEGURADORA_CONTRARIO
    else:
        if k == ENTIDAD_ASEGURADORA and ENTIDAD_ASEGURADORA_ASEGURADO in found_fields_by_page.keys():
            k = ENTIDAD_ASEGURADORA_CONTRARIO
        elif k == ENTIDAD_ASEGURADORA and ENTIDAD_ASEGURADORA_ASEGURADO not in found_fields_by_page.keys():
            k = ENTIDAD_ASEGURADORA_ASEGURADO

    if k == DON_DONA or k == FDO_ or k == DON_DOFIA:
        k = NOMBRE_ASEGURADO

    if k == MARCAMODELO_VEHICULO_ASEGURADO or k == MARCA_VEHICULO_ASEGURADO:
        k = MARCA_VEHICULO_ASEGURADO
    return k


def pass_validation(fields):
    all_keys = fields.keys()
    print(all_keys)
    return (
            FECHA_OCURRENCIA in all_keys and
            PROVINCIA_ACCIDENTE in all_keys and
            MATRICULA_ASEGURADO in all_keys and
            MATRICULA_CONTRARIO in all_keys and
            NOMBRE_ASEGURADO in all_keys and
            ENTIDAD_ASEGURADORA_ASEGURADO in all_keys and
            MARCA_VEHICULO_ASEGURADO in all_keys and
            ENTIDAD_ASEGURADORA_CONTRARIO in all_keys
    )

def run(file_name):
    response = {}
    
    with open(file_name, 'r') as document:
        response = json.loads(document.read())

    doc = Document(response)
    fields = processDocument(doc)
    is_valid = pass_validation(fields)
    print(json.dumps(fields, indent = 4))
    print("IS VALID: " + str(is_valid))

if __name__ == "__main__":
    #file_name = sys.argv[1]
    file_name = "/home/valen/Descargas/apiResponse2.json"
    run(file_name)
    file_name = "/home/valen/Descargas/apiResponse3.json"
    run(file_name)
    file_name = "/home/valen/Descargas/apiResponse4.json"
    run(file_name)
    file_name = "/home/valen/Descargas/apiResponse5.json"
    run(file_name)
    file_name = "/home/valen/Descargas/apiResponse6.json"
    run(file_name)
    file_name = "/home/valen/Descargas/apiResponse7.json"
    run(file_name)
