import csv
import shutil
import boto3

import sys
sys.path.append('..')

from os.path import join, basename
from os import walk, path, mkdir, environ
from lib.trp import Document

def create_comprehend_training_file(image_files_path, text_files_path, aws_profile):
    print("\nStarted processing files from: {0}".format(image_files_path))
    print("------------------------------------\n")
    environ["AWS_PROFILE"] = aws_profile
    if path.isdir(text_files_path):
        shutil.rmtree(text_files_path)

    replicate_folder_structure(image_files_path, text_files_path)
    extract_text_from_images(image_files_path, text_files_path)
    generate_training_file(text_files_path)
    print("\n------------------------------------")
    print("Training file generated successfully")

def extract_text_from_images(image_files_path, text_files_path):
    client = boto3.client('textract')
    for root, dirs, files in walk(image_files_path):
        for sample in files:
            print(join(basename(root), sample))
            with open(join(root, sample), 'rb') as document:
                image_bytes = bytearray(document.read())
                response = client.detect_document_text(
                    Document={
                        'Bytes': image_bytes
                    }
                )
                doc = Document(response)
                new_name = sample.replace('png', 'txt').replace('PNG', 'txt').replace('jpg', 'txt')\
                    .replace('JPG', 'txt').replace('jpeg', 'txt').replace('JPEG', 'txt')
                with open(join(text_files_path, basename(root), new_name), 'w') as fwd:
                    for page in doc.pages:
                        for line in page.lines:
                            fwd.write(line.text)
                fwd.close()
            document.close()


def replicate_folder_structure(input_path, output_path):
    if not path.isdir(output_path):
        mkdir(output_path)
    for dirpath, dirnames, filenames in walk(input_path):
        if not filenames:
            continue
        structure = path.join(output_path, path.basename(dirpath))
        if not path.isdir(structure):
            mkdir(structure)


def generate_training_file(path_to_files):
    """

    :param path_to_files:
    """
    f_writ = open('training_data.csv', 'w', encoding='utf-8')
    writer = csv.writer(f_writ, delimiter=',', lineterminator='\n', quoting=csv.QUOTE_ALL)
    for root, dirs, files in walk(path_to_files):
        for sample in files:
            with open(join(root, sample), 'r', encoding='utf-8', errors='ignore') as fd:
                text = fd.read()
                label = basename(root)
                writer.writerow([label, text.replace('\n', ' ')])
            fd.close()


if __name__ == "__main__":
    """
    Expects folder structure for imageFilesPath like:
           category1/file1.png
                    /file2.jpg
           category2/file3.jpeg
           category3/file4.PNG
    Where files contains the text you want to train for each category
    ¡It only accepts images types png, jpg and jpeg!
    Result will be a csv file compatible for training with AWS Comprehend which include the extracted text
    from each message with the associated label extracted from the folder name     
    """

    image_files_path = "PATH TO IMAGES FOLDERS"
    text_files_path = "/tmp/comprehendTextFiles"
    aws_profile = ""

    create_comprehend_training_file(image_files_path, text_files_path, aws_profile)
